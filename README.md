# Wasteland Warfare
VR team project
![image](https://gitlab.com/parisrice85/unity_vr/uploads/0a1f782b4f783a98401763539579131b/013.png)  
장르	VR Shooter

개발 일정	

    2023.04.10 ~ 2023.04.13

게임 플랫폼

	Meta Quest2 (Android7.0’Nougat’(API Level24)이상)
사용 프로그램	

	Unity(유니티)2023.3.22f1, Visual Studio 17.4.4

핵심 기술

    멀미 발생을 줄이기 위해 Snap Turn Provider을 사용 45도 각도 전환 구현
    Anchor기반의 Teleportation Provider를 사용한 플레이어 이동 및 엄폐 구현 
    Action Based Controller Manager을 사용한 손 애니메이션 구현
    XR Socket Interactor을 사용한 총 배치 구현 
    XR Grab Interactor을 사용하여 Ray를 쏘아 아이템과 상호작용이 가능하게끔 구현

게임 조작방법

	화면 전환: Oculus Controller 이동 
    총 줍기: RawAxis1D.(R, L)Hand Trigger 누르고 있기 
    총 버리기: RawAxis1D.(R, L)Hand Trigger 떼기
    화면 방향 전환: RawAxis2D.(R, L)Thumb stick 조작 
    공격: (총을 쥐고 있다면) RawAxis1D.(R, L)LindexTrriger 누르기
    텔레포트(이동): 텔레포트 오브젝트에 컨트롤러를 가져다 놓고 흰색으로 변하면 RawAxis1D.(R, L)LindexTrriger 누르기 

![image](https://gitlab.com/parisrice85/unity_vr/uploads/fb874289b4d9d1199637c5ec80e6b737/010.png)  
